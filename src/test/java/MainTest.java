import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MainTest {

    // TODO PLEASE NOTE THAT GOOGLE CHROME VERSION (AND CHROME DRIVE RESPECTIVELY) IS 90.0.4430.24

    @Test
    public void Tasks() throws AWTException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        options.addArguments("disable-infobars");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        WebDriver driver = new ChromeDriver(options);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        Actions hold = new Actions(driver);
        driver.manage().window().maximize();
        File testImage = new File("src\\test\\resources\\testlogo.png");

        driver.get("http://the-internet.herokuapp.com/");
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#content > ul > li:nth-child(18) > a")).click();             // using cssSelector - Task 1
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);                          // using other functionality of driver.manage method - Task 9
        driver.findElement(By.xpath("//*[@id=\"file-upload\"]")).sendKeys(testImage.getAbsolutePath()); // using Xpath - Task 1
        driver.findElement(By.className("button")).click();                                             // using className - Task 1
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("uploaded-files")));
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        assertTrue(driver.findElement(By.id("uploaded-files")).isDisplayed());                          // verifying that file is uploaded, task completed: Upload a file - Task 10, part 1/2.

        // TODO Back to main page.

        driver.navigate().back();
        driver.navigate().back();

        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#content > ul > li:nth-child(17) > a")).click();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        assertTrue(driver.findElement(By.partialLinkText("chromedriver.exe")).isDisplayed());              // verifying that file to download is displayed + using partialLinkText - Task 1
        driver.findElement(By.partialLinkText("chromedriver.exe")).click();                                // downloading file, task completed: Upload a file - Task 10, part 2/2.
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body")));

        // TODO Back to main page.

        driver.navigate().back();

        driver.findElement(By.xpath("//*[@id=\"content\"]/ul/li[11]/a")).click();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        Select dropdown = new Select(driver.findElement(By.id("dropdown")));
        dropdown.selectByValue("1");
        assertTrue(driver.findElement(By.cssSelector("#dropdown > option:nth-child(2)")).isSelected());  // Task 5 & Task 6 are implemented & verified.

        // TODO Back to main page.

        driver.navigate().back();

        driver.findElement(By.xpath("//*[@id=\"content\"]/ul/li[22]/a")).click();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.findElement(By.partialLinkText("iFrame")).click();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.className("tox-edit-area__iframe")));
        driver.switchTo().frame(0);
        driver.findElement(By.cssSelector("#tinymce > p")).clear();
        driver.findElement(By.cssSelector("#tinymce > p")).sendKeys("YOU WILL SEE TEST TEXT HERE FROM IBA-TECH");       // Task 3 & Task 4 are implemented.
        driver.switchTo().defaultContent();

        // TODO Back to main page.

        driver.navigate().back();
        driver.navigate().back();

        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.findElement(By.partialLinkText("JavaScript Alerts")).click();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#content > div > ul > li:nth-child(2) > button")).click();
        driver.switchTo().alert().accept();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("result")));
        String result = "You clicked: Ok";
        assertEquals(result, driver.findElement(By.id("result")).getText());      // Task 7 is implemented & verified.

        // TODO Back to main page.

        driver.navigate().back();
        driver.findElement(By.partialLinkText("JQuery UI Menus")).click();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        hold.moveToElement(driver.findElement(By.id("ui-id-2"))).perform();
        driver.findElement(By.id("ui-id-5")).click();
        String expectedURL = "http://the-internet.herokuapp.com/jqueryui";
        assertEquals(expectedURL, driver.getCurrentUrl());                          // Task 8 is implemented & verified.

        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body")));

        //TODO Opening another tab to perform

        Robot r = new Robot();
        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_T);
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyRelease(KeyEvent.VK_T);

        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body")));
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.get("https://turbo.az/");
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("body > div.body-inner > div.header > div.header-tabs-container > div > a.header-tabs-i.header-tabs-i_bina")).click();
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body")));
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(2));
        String expectedURLfromTurboAz = "https://bina.az/?utm_source=turboaz&utm_medium=desktop-nav&utm_campaign=only-logo";
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        assertEquals(expectedURLfromTurboAz, driver.getCurrentUrl());                               // Task 2 is implemented & verified
    }

}